package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.AcnespotmoleBean;
import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.modules.weixin.po.WXAccessToken;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.bodyanalysis.AipBodyAnalysis;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.util.HashMap;

/**
 * @author 小帅丶
 * @className IdPhotoRestController
 * @Description 证件照处理
 * @Date 2020/5/21-10:20
 **/
@Controller
@RequestMapping(value = "/rest/idphoto")
@Scope("prototype")
@Slf4j
@Api(tags = "证件照处理-API")
public class IdPhotoRestController extends ApiRestController {

    AipBodyAnalysis aipBodyAnalysis = BDFactory.getAipBodyAnalysis();
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    /**
     * @Description 图片背景色修改接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年4月26日
     **/
    @RequestMapping(value = "/replace", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> idphotoReplace(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        AcnespotmoleBean bean = new AcnespotmoleBean();
        WXAccessToken imgCheckBean = null;
        //颜色
        String colorStr = ServletRequestUtils.getStringParameter(request, "color","red");
        HashMap<String, String> options = new HashMap<>();
        try {
            startTime = System.currentTimeMillis();
            options.put("type", "foreground");
            param = "image="+ Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId+",type=foreground";
            JSONObject object = aipBodyAnalysis.bodySeg(file.getBytes(), options);
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealIDPhotoBean(object,file,colorStr);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealIDPhotoBean(object,file,colorStr);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("背景图变化接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("图片背景色修改接口耗时{}",timeConsuming);
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.PHOTO_REPALCE_BGCOLOR,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
}
