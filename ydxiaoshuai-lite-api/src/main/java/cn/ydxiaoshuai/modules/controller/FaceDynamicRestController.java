package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.oConvertUtils;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import cn.ydxiaoshuai.modules.facedynamic.service.IFaceDynamicTaskService;
import cn.ydxiaoshuai.modules.vo.FaceDriverVirtualPage;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author 小帅丶
 * @className FaceDynamicRestController
 * @Description 任务列表查询接口
 * @Date 2020/4/10-14:49
 **/
@Controller
@RequestMapping(value = "/rest/face_dynamic")
@Scope("prototype")
@Slf4j
@Api(tags = "任务列表查询-API")
public class FaceDynamicRestController extends ApiRestController {
    @Autowired
    private IFaceDynamicTaskService faceDynamicTaskService;

    /**
     * 分页列表查询
     *
     * @param faceDynamicTask
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "人脸动态任务-带参分页列表查询")
    @ApiOperation(value="人脸动态任务-带参分页列表查询", notes="人脸动态任务-带参分页列表查询")
    @GetMapping(value = "/list")
    public ResponseEntity<Object> queryPageList(FaceDynamicTask faceDynamicTask,
                                                @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                                @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        FaceDriverVirtualPage bean = new FaceDriverVirtualPage();
        if(oConvertUtils.isEmpty(faceDynamicTask.getUserId())){
            bean.fail("invalid params","必要参数缺失",410101);
        }else{
            LambdaQueryWrapper<FaceDynamicTask> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.select(FaceDynamicTask::getUserId,FaceDynamicTask::getApiType,
                    FaceDynamicTask::getTaskId,FaceDynamicTask::getTaskStatus,FaceDynamicTask::getVideoUrl);
            queryWrapper.eq(FaceDynamicTask::getUserId, faceDynamicTask.getUserId());
            if(oConvertUtils.isNotEmpty(faceDynamicTask.getApiType())){
                queryWrapper.eq(FaceDynamicTask::getApiType, faceDynamicTask.getApiType());
            }
            queryWrapper.orderByDesc(FaceDynamicTask::getCreateTime);
            Page<FaceDynamicTask> page = new Page<FaceDynamicTask>(pageNo, pageSize);
            IPage<FaceDynamicTask> pageList = faceDynamicTaskService.page(page, queryWrapper);
            bean.success("success", "查询成功",pageList);
        }
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
}
