package cn.ydxiaoshuai.modules.faceeffects.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.common.util.oss.TencentOssUtil;
import cn.ydxiaoshuai.modules.faceeffects.entity.FaceEffectsTransferTemplate;
import cn.ydxiaoshuai.modules.faceeffects.service.IFaceEffectsTransferTemplateService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 虚拟换妆模板图表
 * @Author: 小帅丶
 * @Date:   2020-09-04
 * @Version: V1.0
 */
@Slf4j
@Api(tags="虚拟换妆模板图表")
@RestController
@RequestMapping("/faceeffects/faceEffectsTransferTemplate")
public class FaceEffectsTransferTemplateController extends JeecgController<FaceEffectsTransferTemplate, IFaceEffectsTransferTemplateService> {
	@Autowired
	private IFaceEffectsTransferTemplateService faceEffectsTransferTemplateService;
	 @Autowired
	 private TencentOssUtil tencentOssUtil;
	 /**文件夹路径*/
	 private static String PATH = "transfer";
	/**
	 * 分页列表查询
	 *
	 * @param faceEffectsTransferTemplate
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-分页列表查询")
	@ApiOperation(value="虚拟换妆模板图表-分页列表查询", notes="虚拟换妆模板图表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FaceEffectsTransferTemplate faceEffectsTransferTemplate,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FaceEffectsTransferTemplate> queryWrapper = QueryGenerator.initQueryWrapper(faceEffectsTransferTemplate, req.getParameterMap());
		Page<FaceEffectsTransferTemplate> page = new Page<FaceEffectsTransferTemplate>(pageNo, pageSize);
		IPage<FaceEffectsTransferTemplate> pageList = faceEffectsTransferTemplateService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param faceEffectsTransferTemplate
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-添加")
	@ApiOperation(value="虚拟换妆模板图表-添加", notes="虚拟换妆模板图表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FaceEffectsTransferTemplate faceEffectsTransferTemplate) {
		faceEffectsTransferTemplateService.save(faceEffectsTransferTemplate);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param faceEffectsTransferTemplate
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-编辑")
	@ApiOperation(value="虚拟换妆模板图表-编辑", notes="虚拟换妆模板图表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FaceEffectsTransferTemplate faceEffectsTransferTemplate) {
		faceEffectsTransferTemplateService.updateById(faceEffectsTransferTemplate);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-通过id删除")
	@ApiOperation(value="虚拟换妆模板图表-通过id删除", notes="虚拟换妆模板图表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		FaceEffectsTransferTemplate transferTemplateDB = faceEffectsTransferTemplateService.getById(id);
		if(null!=transferTemplateDB){
			tencentOssUtil.del(PATH, transferTemplateDB.getImgName());
			faceEffectsTransferTemplateService.removeById(id);
			return Result.ok("删除成功!");
		}else{
			faceEffectsTransferTemplateService.removeById(id);
			return Result.ok("删除成功!");
		}
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-批量删除")
	@ApiOperation(value="虚拟换妆模板图表-批量删除", notes="虚拟换妆模板图表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		LambdaQueryWrapper<FaceEffectsTransferTemplate> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.in(FaceEffectsTransferTemplate::getId, Arrays.asList(ids.split(",")));
		List<FaceEffectsTransferTemplate> list = faceEffectsTransferTemplateService.list(queryWrapper);
		if(list.size()>0){
			list.forEach(fmdb->{
				tencentOssUtil.del(PATH, fmdb.getImgName());
			});
		}
		this.faceEffectsTransferTemplateService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "虚拟换妆模板图表-通过id查询")
	@ApiOperation(value="虚拟换妆模板图表-通过id查询", notes="虚拟换妆模板图表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FaceEffectsTransferTemplate faceEffectsTransferTemplate = faceEffectsTransferTemplateService.getById(id);
		return Result.ok(faceEffectsTransferTemplate);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param faceEffectsTransferTemplate
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, FaceEffectsTransferTemplate faceEffectsTransferTemplate) {
      return super.exportXls(request, faceEffectsTransferTemplate, FaceEffectsTransferTemplate.class, "虚拟换妆模板图表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, FaceEffectsTransferTemplate.class);
  }

}
