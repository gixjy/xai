package cn.ydxiaoshuai.modules.facedynamic.service.impl;

import cn.ydxiaoshuai.common.api.vo.api.FaceDriverVirtualResponseBean;
import cn.ydxiaoshuai.common.constant.StatusConts;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import cn.ydxiaoshuai.modules.facedynamic.mapper.FaceDynamicTaskMapper;
import cn.ydxiaoshuai.modules.facedynamic.service.IFaceDynamicTaskService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;

/**
 * @Description: 人脸动态任务
 * @Author: 小帅丶
 * @Date:   2021-05-13
 * @Version: V1.0
 */
@Service
public class FaceDynamicTaskServiceImpl extends ServiceImpl<FaceDynamicTaskMapper, FaceDynamicTask> implements IFaceDynamicTaskService {

    /**
     * @Author 小帅丶
     * @Description 保存数据
     * @Date  2021-05-13 17:33
     * @param userId - 用户ID
     * @param responseBean - 接口返回内容
     * @param logType  - 接口类型
     * @return void
     **/
    @Override
    public void saveByTaskId(String userId, FaceDriverVirtualResponseBean responseBean,Integer logType) {
        FaceDynamicTask faceDynamicTask = new FaceDynamicTask();
        faceDynamicTask.setApiType(String.valueOf(logType));
        faceDynamicTask.setCreateBy(userId);
        faceDynamicTask.setCreateTime(new Date());
        faceDynamicTask.setUserId(userId);
        faceDynamicTask.setTaskId(responseBean.getResult().getTask_id());
        faceDynamicTask.setTaskStatus(StatusConts.COMMIT);
        baseMapper.insert(faceDynamicTask);
    }
    /**
     * @Author 小帅丶
     * @Description 根据任务查询
     * @Date  2021年5月13日17:56:48
     * @param taskId - 任务ID
     * @return FaceDynamicTask
     **/
    @Override
    public FaceDynamicTask getOneByTaskId(String taskId) {
        LambdaQueryWrapper<FaceDynamicTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FaceDynamicTask::getTaskId,taskId);
        FaceDynamicTask faceDynamicTask = baseMapper.selectOne(queryWrapper);
        return faceDynamicTask;
    }
    /**
     * @Author 小帅丶
     * @Description 根据用户id查询具体接口体验次数
     * @Date  2021年5月17日09:37:59
     * @param userId - 任务ID
     * @param logType - 日志类型
     * @return FaceDynamicTask
     **/
    @Override
    public Integer getCountByUserId(String userId, Integer logType) {
        LambdaQueryWrapper<FaceDynamicTask> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FaceDynamicTask::getUserId,userId)
        .eq(FaceDynamicTask::getApiType,logType)
        .eq(FaceDynamicTask::getTaskStatus,StatusConts.SUCCESS);
        List<FaceDynamicTask> faceDynamicTasks = baseMapper.selectList(queryWrapper);
        if(faceDynamicTasks.isEmpty()){
            return 0;
        }else{
            return faceDynamicTasks.size();
        }
    }
}
