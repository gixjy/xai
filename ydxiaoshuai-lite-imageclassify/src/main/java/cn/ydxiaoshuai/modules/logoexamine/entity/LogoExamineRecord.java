package cn.ydxiaoshuai.modules.logoexamine.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: LOGO自定义上传审核记录表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("imageclassify_logo_examine_record")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="imageclassify_logo_examine_record对象", description="LOGO自定义上传审核记录表")
public class LogoExamineRecord {
    
	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
	private String id;
	/**图片OSS路径*/
	@Excel(name = "图片OSS路径", width = 15)
    @ApiModelProperty(value = "图片OSS路径")
	private String logoUrl;
	/**LOGO名称*/
	@Excel(name = "LOGO名称", width = 15)
    @ApiModelProperty(value = "LOGO名称")
	private String logoName;
	/**LOGO编号系统生成*/
	@Excel(name = "LOGO编号", width = 15)
    @ApiModelProperty(value = "LOGO编号")
	private String logoCode;
	/**添加接口返回的标识*/
	@Excel(name = "添加接口返回的标识", width = 15)
    @ApiModelProperty(value = "添加接口返回的标识")
	private String contSignAdd;
	/**删除返回的标识*/
	@Excel(name = "删除返回的标识", width = 15)
    @ApiModelProperty(value = "删除返回的标识")
	private String contSignDel;
	/**用户系统ID*/
	@Excel(name = "用户系统ID", width = 15)
    @ApiModelProperty(value = "用户系统ID")
	private String userId;
	/**审核状态 0待审核 1审核通过 2审核失败*/
	@Excel(name = "审核状态", width = 15)
    @ApiModelProperty(value = "审核状态")
	private Integer examinStatus;
	/**0待上传 1上传到图库 2拒绝上传*/
	private Integer logoStatus;
	/**创建者*/
	@Excel(name = "创建者", width = 15)
    @ApiModelProperty(value = "创建者")
	private String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新者*/
	@Excel(name = "更新者", width = 15)
    @ApiModelProperty(value = "更新者")
	private String updateBy;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
	private String remark;
}
