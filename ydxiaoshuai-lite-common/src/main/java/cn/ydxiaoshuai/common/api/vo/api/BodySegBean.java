package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className BodySegBean
 * @Description 人像分割
 * @Date 2020/5/22-15:18
 **/
@NoArgsConstructor
@Data
public class BodySegBean {

    private int person_num;
    private String foreground;
    private long log_id;
    private List<PersonInfoBean> person_info;

    @NoArgsConstructor
    @Data
    public static class PersonInfoBean {

        private double height;
        private double width;
        private double top;
        private double score;
        private double left;
    }
}
