package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @author 小帅丶
 * @className EasyDLResponseBean
 * @Description EasyDL对象
 * @Date 2020/3/12-11:19
 **/
@Data
public class EasyDLChinesResponseBean {
    //拼音
    private String medicine_pinyin;
    //中文名称
    private String medicine_name;
    //别名
    private String medicine_alias;
    //药味
    private String medicine_taste;
    //归经
    private String medicine_meridian_tropism;
    //功能类型
    private String medicine_category;
    //功能
    private String medicine_function;
    //置信度
    private String score;
    //药用部位
    private String medicinal_parts;
}
