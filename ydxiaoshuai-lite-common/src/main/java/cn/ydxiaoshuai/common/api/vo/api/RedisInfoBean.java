package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @Description redis更新返回的内容
 * @author 小帅丶
 * @className RedisInfoBean
 * @Date 2020年08月13日11:35:28
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedisInfoBean extends BaseBean{
    //具体返回内容
    private Data data;
    private List<Object> list;
    @lombok.Data
    public static class Data{
        private String key_name;
        private String key_value;
        private boolean update_status;
        private String face_data;
    }
    public RedisInfoBean success(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        return this;
    }
    public RedisInfoBean success(String msg,String msg_zh, List<Object> list) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.list = list;
        return this;
    }
    public RedisInfoBean success(String msg,String msg_zh, Data data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public RedisInfoBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public RedisInfoBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.code =500;
        return this;
    }
    public RedisInfoBean unknow(String msg,String msg_zh, Integer code, Data data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.code = code;
        this.data = data;
        return this;
    }
}
